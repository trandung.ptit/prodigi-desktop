﻿namespace Prodigi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_1 = new System.Windows.Forms.TabPage();
            this.btn_stop_bat_so = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.diachiCBB = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.traSauRB = new System.Windows.Forms.RadioButton();
            this.traTruocRB = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.datetime_picker_stop = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.datetime_picker_start = new System.Windows.Forms.DateTimePicker();
            this.delayTB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_start_bat_so = new System.Windows.Forms.Button();
            this.phonesFailTB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.phonesSuccessTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.phonesTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.logsTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.contactTB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.passwordTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.log_kt_that_bai = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.log_kt_thanh_cong = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.khoitaoTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tab_1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-3, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(669, 450);
            this.tabControl1.TabIndex = 15;
            // 
            // tab_1
            // 
            this.tab_1.BackColor = System.Drawing.Color.Beige;
            this.tab_1.Controls.Add(this.btn_stop_bat_so);
            this.tab_1.Controls.Add(this.label11);
            this.tab_1.Controls.Add(this.diachiCBB);
            this.tab_1.Controls.Add(this.groupBox2);
            this.tab_1.Controls.Add(this.groupBox1);
            this.tab_1.Controls.Add(this.btn_start_bat_so);
            this.tab_1.Controls.Add(this.phonesFailTB);
            this.tab_1.Controls.Add(this.label7);
            this.tab_1.Controls.Add(this.phonesSuccessTB);
            this.tab_1.Controls.Add(this.label6);
            this.tab_1.Controls.Add(this.phonesTB);
            this.tab_1.Controls.Add(this.label5);
            this.tab_1.Controls.Add(this.logsTB);
            this.tab_1.Controls.Add(this.label4);
            this.tab_1.Controls.Add(this.contactTB);
            this.tab_1.Controls.Add(this.label3);
            this.tab_1.Controls.Add(this.passwordTB);
            this.tab_1.Controls.Add(this.label2);
            this.tab_1.Controls.Add(this.usernameTB);
            this.tab_1.Controls.Add(this.label1);
            this.tab_1.Location = new System.Drawing.Point(4, 22);
            this.tab_1.Name = "tab_1";
            this.tab_1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_1.Size = new System.Drawing.Size(661, 424);
            this.tab_1.TabIndex = 0;
            this.tab_1.Text = "Bắt số";
            this.tab_1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btn_stop_bat_so
            // 
            this.btn_stop_bat_so.Enabled = false;
            this.btn_stop_bat_so.Location = new System.Drawing.Point(521, 398);
            this.btn_stop_bat_so.Name = "btn_stop_bat_so";
            this.btn_stop_bat_so.Size = new System.Drawing.Size(62, 23);
            this.btn_stop_bat_so.TabIndex = 37;
            this.btn_stop_bat_so.Text = "Stop";
            this.btn_stop_bat_so.UseVisualStyleBackColor = true;
            this.btn_stop_bat_so.Click += new System.EventHandler(this.btn_stop_bat_so_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(246, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Địa chỉ";
            // 
            // diachiCBB
            // 
            this.diachiCBB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.diachiCBB.FormattingEnabled = true;
            this.diachiCBB.Items.AddRange(new object[] {
            "1. PTO - Đoan Hùng",
            "2. DLC - Ea Phê",
            "3. NBH - Yên Khánh",
            "4. DLC - Krong Buk",
            "5. HUE - Phong Điền",
            "6. HNO - 165 Cầu Giấy",
            "7. LCU - Phong Thổ"});
            this.diachiCBB.Location = new System.Drawing.Point(301, 47);
            this.diachiCBB.Name = "diachiCBB";
            this.diachiCBB.Size = new System.Drawing.Size(132, 21);
            this.diachiCBB.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.traSauRB);
            this.groupBox2.Controls.Add(this.traTruocRB);
            this.groupBox2.Location = new System.Drawing.Point(336, 101);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 56);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Loại";
            // 
            // traSauRB
            // 
            this.traSauRB.AutoSize = true;
            this.traSauRB.Location = new System.Drawing.Point(101, 19);
            this.traSauRB.Name = "traSauRB";
            this.traSauRB.Size = new System.Drawing.Size(61, 17);
            this.traSauRB.TabIndex = 34;
            this.traSauRB.Text = "Trả sau";
            this.traSauRB.UseVisualStyleBackColor = true;
            this.traSauRB.CheckedChanged += new System.EventHandler(this.traSauRB_CheckedChanged);
            // 
            // traTruocRB
            // 
            this.traTruocRB.AutoSize = true;
            this.traTruocRB.Checked = true;
            this.traTruocRB.Location = new System.Drawing.Point(9, 19);
            this.traTruocRB.Name = "traTruocRB";
            this.traTruocRB.Size = new System.Drawing.Size(68, 17);
            this.traTruocRB.TabIndex = 33;
            this.traTruocRB.TabStop = true;
            this.traTruocRB.Text = "Trả trước";
            this.traTruocRB.UseVisualStyleBackColor = true;
            this.traTruocRB.CheckedChanged += new System.EventHandler(this.traTruocRB_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.datetime_picker_stop);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.datetime_picker_start);
            this.groupBox1.Controls.Add(this.delayTB);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(336, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 257);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setting";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Stop";
            // 
            // datetime_picker_stop
            // 
            this.datetime_picker_stop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_stop.Location = new System.Drawing.Point(10, 126);
            this.datetime_picker_stop.Name = "datetime_picker_stop";
            this.datetime_picker_stop.ShowCheckBox = true;
            this.datetime_picker_stop.Size = new System.Drawing.Size(160, 20);
            this.datetime_picker_stop.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Start";
            // 
            // datetime_picker_start
            // 
            this.datetime_picker_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_start.Location = new System.Drawing.Point(10, 71);
            this.datetime_picker_start.Name = "datetime_picker_start";
            this.datetime_picker_start.ShowCheckBox = true;
            this.datetime_picker_start.Size = new System.Drawing.Size(160, 20);
            this.datetime_picker_start.TabIndex = 33;
            // 
            // delayTB
            // 
            this.delayTB.Location = new System.Drawing.Point(62, 20);
            this.delayTB.Name = "delayTB";
            this.delayTB.Size = new System.Drawing.Size(107, 20);
            this.delayTB.TabIndex = 32;
            this.delayTB.Text = "500";
            this.delayTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Delay";
            // 
            // btn_start_bat_so
            // 
            this.btn_start_bat_so.Location = new System.Drawing.Point(586, 398);
            this.btn_start_bat_so.Name = "btn_start_bat_so";
            this.btn_start_bat_so.Size = new System.Drawing.Size(62, 23);
            this.btn_start_bat_so.TabIndex = 29;
            this.btn_start_bat_so.Text = "Start";
            this.btn_start_bat_so.UseVisualStyleBackColor = true;
            this.btn_start_bat_so.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // phonesFailTB
            // 
            this.phonesFailTB.Location = new System.Drawing.Point(521, 298);
            this.phonesFailTB.MaxLength = 32767213;
            this.phonesFailTB.Multiline = true;
            this.phonesFailTB.Name = "phonesFailTB";
            this.phonesFailTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.phonesFailTB.Size = new System.Drawing.Size(127, 68);
            this.phonesFailTB.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(518, 282);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Thất bại";
            // 
            // phonesSuccessTB
            // 
            this.phonesSuccessTB.Location = new System.Drawing.Point(521, 211);
            this.phonesSuccessTB.Multiline = true;
            this.phonesSuccessTB.Name = "phonesSuccessTB";
            this.phonesSuccessTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.phonesSuccessTB.Size = new System.Drawing.Size(127, 68);
            this.phonesSuccessTB.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(518, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Thành công";
            // 
            // phonesTB
            // 
            this.phonesTB.Location = new System.Drawing.Point(521, 27);
            this.phonesTB.Multiline = true;
            this.phonesTB.Name = "phonesTB";
            this.phonesTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.phonesTB.Size = new System.Drawing.Size(127, 162);
            this.phonesTB.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(518, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Số cần bắt";
            // 
            // logsTB
            // 
            this.logsTB.Location = new System.Drawing.Point(11, 104);
            this.logsTB.Multiline = true;
            this.logsTB.Name = "logsTB";
            this.logsTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logsTB.Size = new System.Drawing.Size(311, 317);
            this.logsTB.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Log";
            // 
            // contactTB
            // 
            this.contactTB.Location = new System.Drawing.Point(69, 44);
            this.contactTB.Name = "contactTB";
            this.contactTB.Size = new System.Drawing.Size(127, 20);
            this.contactTB.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Sđt liên hệ";
            // 
            // passwordTB
            // 
            this.passwordTB.Location = new System.Drawing.Point(301, 4);
            this.passwordTB.Name = "passwordTB";
            this.passwordTB.Size = new System.Drawing.Size(132, 20);
            this.passwordTB.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(242, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Password";
            // 
            // usernameTB
            // 
            this.usernameTB.Location = new System.Drawing.Point(69, 4);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.Size = new System.Drawing.Size(127, 20);
            this.usernameTB.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Username";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.log_kt_that_bai);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.log_kt_thanh_cong);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.khoitaoTB);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(661, 424);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Khởi tạo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // log_kt_that_bai
            // 
            this.log_kt_that_bai.Location = new System.Drawing.Point(345, 44);
            this.log_kt_that_bai.Multiline = true;
            this.log_kt_that_bai.Name = "log_kt_that_bai";
            this.log_kt_that_bai.Size = new System.Drawing.Size(307, 371);
            this.log_kt_that_bai.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(342, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Log thất bại";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(536, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Start";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // log_kt_thanh_cong
            // 
            this.log_kt_thanh_cong.Location = new System.Drawing.Point(247, 44);
            this.log_kt_thanh_cong.Multiline = true;
            this.log_kt_thanh_cong.Name = "log_kt_thanh_cong";
            this.log_kt_thanh_cong.Size = new System.Drawing.Size(92, 371);
            this.log_kt_thanh_cong.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(244, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Log thành công";
            // 
            // khoitaoTB
            // 
            this.khoitaoTB.Location = new System.Drawing.Point(11, 44);
            this.khoitaoTB.Multiline = true;
            this.khoitaoTB.Name = "khoitaoTB";
            this.khoitaoTB.Size = new System.Drawing.Size(230, 371);
            this.khoitaoTB.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "SĐT-Mã giữ-Seri sim";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 450);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Prodigi by TD v1.0.20";
            this.tabControl1.ResumeLayout(false);
            this.tab_1.ResumeLayout(false);
            this.tab_1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_start_bat_so;
        private System.Windows.Forms.TextBox phonesFailTB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox phonesSuccessTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox phonesTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox logsTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox contactTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox passwordTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox log_kt_thanh_cong;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox khoitaoTB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox delayTB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton traSauRB;
        private System.Windows.Forms.RadioButton traTruocRB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox diachiCBB;
        private System.Windows.Forms.TextBox log_kt_that_bai;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_stop_bat_so;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker datetime_picker_stop;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker datetime_picker_start;
    }
}

