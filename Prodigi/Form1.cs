﻿using System;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;

namespace Prodigi
{
    public partial class Form1 : Form
    {

        delegate void SetTextCallback(string text);
        private delegate object getComoboItem();
        private static readonly object _lock = new object();
        public Form1()
        {
            InitializeComponent();
            readSetting();
            datetime_picker_start.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";
            datetime_picker_stop.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";


            datetime_picker_start.Value = new DateTime(
                datetime_picker_start.Value.Year,
                datetime_picker_start.Value.Month,
                datetime_picker_start.Value.Day, 2, 04, 59);

            datetime_picker_stop.Value = new DateTime(
                datetime_picker_stop.Value.Year,
                datetime_picker_stop.Value.Month,
                datetime_picker_stop.Value.Day, 2, 14, 59);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] phones = phonesTB.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );



            for (int i = 0; i < phones.Length; i++)
            {
                logsTB.AppendText(phones[i]);

            }
            //logsTB.AppendText("First");

            //Thread t = new Thread(() => {
            //    while (lapCB.Checked)
            //    {
            //        for (int i = 0; i < phones.Length; i++)
            //        {
            //            SetLogTB(phones[i]);
            //            SetLogTB(Environment.NewLine);
            //        }
            //    }
            //});
            //t.Start();



        }
        string getTokenLoginDigi(string data)
        {
            string token = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=access_token"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                token = res[0].ToString();
            }
            return token;
        }

        private async void button1_Click_1(object sender, EventArgs e)
        {
            btn_stop_bat_so.Enabled = true;
            btn_start_bat_so.Enabled = false;
            saveSetting();
            phonesFailTB.Text = string.Empty;
            string username = usernameTB.Text;
            string password = passwordTB.Text;
            string[] phones = phonesTB.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            string phoneSplited= string.Empty;
            for(int i = 0; i < phones.Length; i++)
            {
                string outres = phones[i] + " ";
                phoneSplited += outres;
            }

            HttpClient client = new HttpClient();
            string mxtRequest = $@"{{
                                          ""username"": ""{username}"",
                                          ""password"": ""{password}"",
                                          ""type"": ""PLACE_ORDER"",
                                          ""content"": ""{phoneSplited.Trim()}"",
                                          ""sdt_nhan_ma"": ""{contactTB.Text}"",
                                          ""loai_thue_bao"": ""{getLoaiTBString()}""
                                        }}";
            var mxtResponse = await callMXTAsync(client, "http://provnpt.com:8081/api/v1/maxacthuc", mxtRequest);
            client.DefaultRequestHeaders.Add("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu");
            var loginResponse = await callPostThread(client, string.Format("https://shop-ctv.vnpt.vn/api/web/index.php/user/login?username={0}&password={1}", username, password), "");
            string token = getTokenLoginDigi(loginResponse);
            if (string.IsNullOrEmpty(token))
            {
                SetLogTB(string.Format("Loi dang nhap, {0}-{1}.", username, password));
                return;
            } else
            {
                SetLogTB(string.Format("Dang nhap thanh cong, {0}-{1}.", username, password));
            }
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            bool isUsingAlarm = datetime_picker_start.Checked;
            Thread t = new Thread(() => {
                if (isUsingAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_start.Value;
                    SetLogTB(string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    Thread.Sleep(time_to.Subtract(time_from));
                }
                while (btn_stop_bat_so.Enabled && !string.IsNullOrWhiteSpace(phonesTB.Text))
                {
                    phones = phonesTB.Text.Split(
                        new string[] { Environment.NewLine },
                        StringSplitOptions.RemoveEmptyEntries
                    );
                    Thread.Sleep(int.Parse(delayTB.Text));
                    runMultiplePhone(client, phones);
                }
                if(string.IsNullOrWhiteSpace(phonesTB.Text))
                {
                    onClickStop();
                    SetLogTB("Đã bắt hết kho.");
                }
            });
            t.Start();



            bool isUsingStopAlarm = datetime_picker_stop.Checked;
            Thread y = new Thread(() =>
            {
                if (isUsingStopAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_stop.Value;
                    Thread.Sleep(time_to.Subtract(time_from));
                    Console.WriteLine(time_to.Subtract(time_from));
                    SetLogTB(string.Format("Dừng tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    onClickStop();
                }
            });
            y.Start();
        }

        void onClickStart()
        {
            this.Invoke((MethodInvoker)delegate {
                btn_start_bat_so.Enabled = false;
                btn_stop_bat_so.Enabled = true;
            });
            
        }

        void onClickStop()
        {
            this.Invoke((MethodInvoker)delegate {
                btn_start_bat_so.Enabled = true;
                btn_stop_bat_so.Enabled = false;
            });
        }


        async void runMultiplePhone(HttpClient client, string[] phones)
        {
            for (int i = 0; i < phones.Length; i++)
            {
                string phone = phones[i].Trim().Replace(".", "");
                if (phone.StartsWith("0")) phone = phone.Substring(1);
                if ((phone.StartsWith("84") && phone.Length < 10) || !phone.StartsWith("84"))
                {
                    phone = "84" + phone;
                }
                try
                {
                    await finalAsync(i, client, phone);
                } catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());   
                }
            }
        }

        void runMultipleStep1(HttpClient client, string[] phones)
        {
            for (int i = 0; i < phones.Length; i++)
            {
                string phone = phones[i].Trim().Replace(".", "");
                if (phone.StartsWith("0")) phone = phone.Substring(1);
                if ((phone.StartsWith("84") && phone.Length < 10) || !phone.StartsWith("84"))
                {
                    phone = "84" + phone;
                }
                try
                {
                    step1Async(i, client, phone);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        async Task finalAsync(int threadIndex, HttpClient client, string phone)
        {
            string booksimRequest = @"{""system"":""IOS"",""stock"":""146"",""msisdn"":""{{phone}}""}".Replace("{{phone}}", phone);
            var booksimResponse = await callPostThread(client, "https://shop-ctv.vnpt.vn/digitalshop/book_sim", booksimRequest);
            string mxt = getMxt(booksimResponse);
            if (string.IsNullOrEmpty(mxt))
            {
                SetLogTB(string.Format("Thread {0}. Buoc 1. Giu sim that bai {1} ", threadIndex, phone));
                SetFailTB(phone);
                return;
            }
            else
            {
                string username = usernameTB.Text;
                string password = passwordTB.Text;
                string mxtRequest = $@"{{
                                          ""username"": ""{username}"",
                                          ""password"": ""{password}"",
                                          ""type"": ""TOKEN_SUCCESS"",
                                          ""content"": ""{phone + "---" + mxt}"",
                                          ""sdt_nhan_ma"": ""{contactTB.Text}"",
                                          ""loai_thue_bao"": ""{getLoaiTBString()}""
                                        }}";
                var mxtResponse = await callMXTAsync(client, "http://provnpt.com:8081/api/v1/maxacthuc", mxtRequest);
                SetLogTB(string.Format("Thread {0}. Bước 1. Giu sim thanh cong {1} ", threadIndex, phone));
            }

            string chonsimRequest = $@"{{
                                          ""cmt"": ""{getCMT()}"",
                                          ""diachi"": ""{getDiaChi()}"",
                                          ""ma_giu_so"": ""{mxt}"",
                                          ""ma_tb"": ""{phone}"",
                                          ""fullname"": ""{getRandomName()} {0 + phone.Substring(2)}"",
                                          ""so_dt"": ""{contactTB.Text}"",
                                          ""product_id"": 0,
                                          ""phi_hoa_mang"": {getPhiHoaMang()},
                                          ""phonggd"": {getPhongGD()},
                                          ""phi_giao_hang"": 0,
                                          ""quan_it_id"": {getQuanITID()},
                                          ""quan_id"": {getQuanID()},
                                          ""cuoc_camket"": 0,
                                          ""gia_sim"": 12500,
                                          ""loaisim"": 2,
                                          ""tinh_id"": {getTinhId()},
                                          ""gia_goi"": 0,
                                          ""ma_kit"": """",
                                          ""ht_nhansim"": 2,
                                          ""ghichu"": """",
                                          ""loaitb_id"": {getLoaiTB()}
                                        }}";
            var chonsimResponse = await callPostThread(client, "https://shop-ctv.vnpt.vn/digitalshop/create_order", chonsimRequest);
            string result = getFinalResult(chonsimResponse);
            if (result != "" && result.Equals("0"))
            {
                // save to db 
                string mxtRequest = $@"{{
                                          ""username"": ""{usernameTB.Text}"",
                                          ""password"": ""{passwordTB.Text}"",
                                          ""type"": ""PLACE_SUCCESS"",
                                          ""content"": ""{phone}"",
                                          ""sdt_nhan_ma"": ""{contactTB.Text}"",
                                          ""loai_thue_bao"": ""{getLoaiTBString()}""
                                        }}
                ";
                callMXTAsync(client, "http://provnpt.com:8081/api/v1/maxacthuc", mxtRequest);


                SetLogTB(string.Format("Thread {0}. Buoc 2. Bat sim thanh cong {1}", threadIndex, phone));
                SetSuccessTB(phone);
                resetPhonesTB(phone);
                rewriteListSo();
            }
            else
            {
                SetLogTB(string.Format("Thread {0}. Buoc 2. Bat sim that bai {1}", threadIndex, phone));
                SetFailTB(phone);
            }
        }

        async void step1Async(int threadIndex, HttpClient client, string phone)
        {
            Console.WriteLine(string.Format("Thread {0}. Bước 0. Chạy giữ số {1} ", threadIndex, phone));
            string booksimRequest = @"{""system"":""IOS"",""stock"":""146"",""msisdn"":""{{phone}}""}".Replace("{{phone}}", phone);
            var booksimResponse = await callPostThread(client, "https://shop-ctv.vnpt.vn/digitalshop/book_sim", booksimRequest);
            string mxt = getMxt(booksimResponse);
            if (string.IsNullOrEmpty(mxt))
            {
                SetLogTB(string.Format("Thread {0}. Buoc 1. Giu sim that bai {1} ", threadIndex, phone));
                SetFailTB(phone);
                return;
            }
            else
            {
                string username = usernameTB.Text;
                string password = passwordTB.Text;
                string mxtRequest = $@"{{
                                          ""username"": ""{username}"",
                                          ""password"": ""{password}"",
                                          ""type"": ""TOKEN_SUCCESS"",
                                          ""content"": ""{phone + "---" + mxt}"",
                                          ""sdt_nhan_ma"": ""{contactTB.Text}"",
                                          ""loai_thue_bao"": ""{getLoaiTBString()}""
                                        }}";
                var mxtResponse = await callMXTAsync(client, "http://provnpt.com:8081/api/v1/maxacthuc", mxtRequest);
                SetLogTB(string.Format("Thread {0}. Bước 1. Giu sim thanh cong {1} ", threadIndex, phone));
            }
        }

        public int getTinhId()
        {
            switch (getSelectedComboItem())
            {
                case 0: return 59;
                case 1: return 16;
                case 2: return 39;
                case 3: return 16;
                case 4: return 53;
                case 5: return 21;
                case 6: return 32;
                default: return 59;
            }
        }

        public string getDiaChi()
        {
            switch (getSelectedComboItem())
            {
                case 0: return "ĐGD_Điểm Giao Dịch Huyện Đoan Hùng";    // Đoan Hùng
                case 1: return "ĐGD_Ea Phê";    // Đắc lắc
                case 2: return "ĐGD_Giao dịch Yên Khánh";   // Ninh Bình
                case 3: return "ĐGD_Krong Búk"; // Đắc Lắc
                case 4: return "ĐGD_Phong Điền";  // Huế
                case 5: return "ĐGD_Cửa hàng 165 Cầu Giấy BH4";  // Cầu Giấy
                case 6: return "ĐGD_Giao dịch Phong Thổ, Huyện Phong Thổ, Lai Châu";
                default: return "ĐGD_Điểm Giao Dịch Huyện Đoan Hùng";
            }
        }

        public int getPhongGD()
        {
            switch (getSelectedComboItem())
            {
                case 0: return 40081;   // Đoan Hùng
                case 1: return 40091;   // Đắc lắc
                case 2: return 13404;   // Ninh Bình
                case 3: return 40093;   // Đắc Lắc 
                case 4: return 40143;   // Huế
                case 5: return 214;   // Cầu Giấy
                case 6: return 474;   // Cầu Giấy
                default: return 40081;
            }
        }

        public int getQuanITID()
        {
            switch (getSelectedComboItem())
            {
                case 0: return 339;
                case 1: return 3;
                case 2: return 3817;
                case 3: return 3600;
                case 4: return 3;
                case 5: return 3664;
                case 6: return 375;
                default: return 339;
            }
        }

        public int getQuanID()
        {
            switch (getSelectedComboItem())
            {
                case 0: return 3302;
                case 1: return 2752;
                case 2: return 3082;
                case 3: return 2749;
                case 4: return 3238;
                case 5: return 2834;
                case 6: return 2988;
                default: return 3302;
            }
        }

        public int getPhiHoaMang()
        {
             if(traTruocRB.Checked)
            {
                return 25000;   //tra truoc
            } else
            {
                return 35000;   //tra sau
            }
        }

        public int getLoaiTB()
        {
            if (traTruocRB.Checked)
            {
                return 21;   //tra truoc
            }
            else
            {
                return 20;   //tra sau
            }
        }

        public String getLoaiTBString()
        {
            if (traTruocRB.Checked)
            {
                return "TRATRUOC";   //tra truoc
            }
            else
            {
                return "TRASAU";   //tra sau
            }
        }

        private object getSelectedComboItem()
        {
            if (diachiCBB.InvokeRequired)
            {
                getComoboItem gci = new getComoboItem(getSelectedComboItem);
                return diachiCBB.Invoke(gci);
            }
            else
                return diachiCBB.SelectedIndex;
        }

        async Task<string> callPostThread(HttpClient client, string url, String content)
        {
            string request = string.Format("Call API: {0}. Content: {1}", url, content);
            HttpResponseMessage loginResponse = new HttpResponseMessage();
            loginResponse.Content = new StringContent("Init content response");
            try
            {
                loginResponse = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json"));
            }
            catch (Exception ex)
            {
                SetLogTB(string.Format("Conntection timeout. Server đang lag"));
            }
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            string response = string.Format("Result API: {0}", loginResponseString);
            return loginResponseString;
        }

         async Task<string> callMXTAsync(HttpClient client, string url, string content)
        {
            var loginResponse = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json"));
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        private void SetLogTB(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.logsTB.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetLogTB);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.logsTB.AppendText(text);
                this.logsTB.AppendText(Environment.NewLine);
            }
        }

        private void resetPhonesTB(string phone)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.phonesTB.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(resetPhonesTB);
                this.Invoke(d, new object[] { phone });
            }
            else
            {
                this.phonesTB.Lines = phonesTB.Lines.Where(line => !line.Contains(phone.Substring(2))).ToArray();    // remove khỏi textbox
            }
        }

        private void SetSuccessTB(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.logsTB.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetSuccessTB);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.phonesSuccessTB.AppendText(text);
                this.phonesSuccessTB.AppendText(Environment.NewLine);
            }
        }

        private void SetFailTB(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.logsTB.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetFailTB);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.phonesFailTB.AppendText(text);
                this.phonesFailTB.AppendText(Environment.NewLine);
            }
        }

        void setKTTCLog(string text)
        {
            if (this.log_kt_thanh_cong.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setKTTCLog);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.log_kt_thanh_cong.AppendText(text);
                this.log_kt_thanh_cong.AppendText(Environment.NewLine);
            }
        }

        void setKTTBLog(string text)
        {
            if (this.log_kt_that_bai.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setKTTBLog);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.log_kt_that_bai.AppendText(text);
                this.log_kt_that_bai.AppendText(Environment.NewLine);
            }
        }



        string getMxt(string booksimResponse)
        {
            string mxt = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(booksimResponse, @"(?<=mxt"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                mxt = res[0].ToString();
            }
            return mxt;
        }

        string getCMT()
        {
            string result = "";
            Random _rdm = new Random();
            for (int i = 0; i < 12; i++)
            {
                result += _rdm.Next(1, 9).ToString();
            }
            return result;
        }

        string getFinalResult(string createOrderResponse)
        {
            string result = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(createOrderResponse, @"(?<=errorCode"":).*?(?=,)", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                result = res[0].ToString();
            }
            return result;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private async void button2_Click(object sender, EventArgs e)
        {
            string listkt = khoitaoTB.Text.Trim();
            string[] lines = listkt.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.None
            );
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu");
            client.DefaultRequestHeaders.Add("User-Agent", "My VNPT/4.3.7 (com.vnp.myvinaphone; build:50; iOS 16.1.0) Alamofire/4.3.7");
            // List<List<string>> chunksOf50 = stuffFromFile.SplitList();
            for (int i = 0;i < lines.Length;i++)
            {
                string[] strings = lines[i].Split();    // split theo khoang trang
                string sdt = strings[0];
                if (sdt.Length == 9)
                {
                    sdt = "84" + sdt;
                }
                else if (sdt.Length == 10)
                {
                    sdt = "84" + sdt.Substring(1, sdt.Length - 1);
                }
                string ma_giu_so = strings[1];
                string seri_sim = strings[2];
                Console.WriteLine(String.Format("sdt {0}. ma giu sim {1}. seri sim {2}", sdt, ma_giu_so, seri_sim));
                string getInfoRequest = $@"{{
                                          ""msisdn"": ""{sdt}"",
                                          ""mxt"": ""{ma_giu_so}""
                                        }}";
                var getInfoResponse = await callPostThread(client, "https://api-myvnpt.vnpt.vn/mapi/services/sim_reg_get_info", getInfoRequest);
                if (!getInfoResponse.Contains("\"error_code\":0,"))
                {
                    setKTTBLog(String.Format("{0}. Loi step 1 tim kiem don hang", sdt));
                    return;
                }
                string season_id = getKhoiTaoTokenStep1(getInfoResponse);
                string khoi_tao_request = $@"{{
                                        ""msisdn"": ""{sdt}"",
                                        ""serial_sim"": ""{seri_sim}"",
                                        ""session"": ""{season_id}"",
                                        ""channel"": ""iOS""
                                            }}";

                var ktResponse = await callPostThread(client, "https://api-myvnpt.vnpt.vn/mapi/services/myvnpt_dktttb_initsim", khoi_tao_request);
                string messagekt = getKhoiTaoTokenStep2(ktResponse);
                if (ktResponse.Contains("\"error_code\":\"0\""))
                {
                    setKTTCLog(sdt);
                }
                else
                {
                    setKTTBLog(String.Format("{0}. Loi step 2. Message: {1}", sdt, messagekt));
                };
            };
        }

        public class AppSetting
        {
            public string username { get; set; }
            public string password { get; set; }
            public string contact { get; set; }
            public string delay { get; set; }
            public string loai_thue_bao { get; set; }
            public string dia_chi_index { get; set; }
            public bool lap { get; set; }
        }

        public void readSetting()
        {
            string fileName = @"setting.txt";
            string fileListPhone = @"list_so.txt";
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(fileName);
                phonesTB.Text = File.ReadAllText(fileListPhone);
            } catch (Exception ex)
            {

            }
            if (string.IsNullOrEmpty(jsonString)) return;
            AppSetting appSetting = JsonSerializer.Deserialize<AppSetting>(jsonString);
            if(appSetting!= null)
            {
                usernameTB.Text = appSetting.username;
                passwordTB.Text = appSetting.password;
                contactTB.Text = appSetting.contact;
                delayTB.Text = appSetting.delay;
                diachiCBB.SelectedIndex = appSetting.dia_chi_index!= null ? int.Parse(appSetting.dia_chi_index) : 0;
                switch (appSetting.loai_thue_bao)
                {
                    case "tra_truoc":
                        traTruocRB.Checked = true; 
                        traSauRB.Checked = false;
                        break;
                    default:
                        traTruocRB.Checked = false;
                        traSauRB.Checked = true;
                        break;
                }
            }
        }

        public void saveSetting()
        {
            AppSetting appSetting = new AppSetting();
            appSetting.username = usernameTB.Text;
            appSetting.password = passwordTB.Text;
            appSetting.contact = contactTB.Text;
            appSetting.delay = delayTB.Text;
            appSetting.loai_thue_bao = traSauRB.Checked ? "tra_sau" : "tra_truoc";
            appSetting.dia_chi_index = diachiCBB.SelectedIndex.ToString();
            string json = JsonSerializer.Serialize(appSetting);

            File.WriteAllText("setting.txt", json);
            File.WriteAllText("list_so.txt", phonesTB.Text);
        }

        void rewriteListSo()
        {
            lock (_lock)
            {
                File.WriteAllText("list_so.txt", phonesTB.Text);
            }
        }

        public String getRandomName()
        {
            String[] list = { "Khang", "Bảo", "Minh", "Phúc" , "Anh", "Khoa" , "Phát" , "Đạt" , "Khôi" , "Long"};
            Random random = new Random();
            int index = random.Next(0, list.Length);
            return list[index];
        }

        string getKhoiTaoTokenStep1(string data)
        {
            string result = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=token"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                result = res[0].ToString();
            }
            return result;
        }

        string getKhoiTaoTokenStep2(string data)
        {
            string result = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=message"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                result = res[0].ToString();
            }
            return result;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void traTruocRB_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    tab_1.BackColor = Color.Beige;
                }
            }
        }

        private void traSauRB_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    tab_1.BackColor = Color.LightSteelBlue;
                }
            }
        }

        private void btn_stop_bat_so_Click(object sender, EventArgs e)
        {
            btn_start_bat_so.Enabled = true;
            btn_stop_bat_so.Enabled = false;
        }

    }
}
